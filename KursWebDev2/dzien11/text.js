const btnBigger = document.querySelector('.bigger');
const btnSmaller = document.querySelector('.smaller');
const text = document.querySelector('p');

let textSize = 16;
text.style.fontSize = textSize + "px";

function textBigger() {
    textSize++;
    text.style.fontSize = textSize + "px";
}

function textSmaller() {
    textSize--;
    text.style.fontSize = textSize + "px";
}

btnBigger.addEventListener('click', textBigger);
btnSmaller.addEventListener('click', textSmaller);
