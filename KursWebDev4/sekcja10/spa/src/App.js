import React, { Component } from "react";
import {
  BrowserRouter as Router,
  NavLink,
  Route,
  Switch
} from "react-router-dom";
import "./App.css";

const Home = () => <h1>Strona Startowa</h1>;
const News = () => <h1>Wiadomości</h1>;
const Contact = () => <h1>Kontakt</h1>;

const ErrorPage = () => <h1>Error 404</h1>;

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <header>
            <nav>
              <ul>
                <li>
                  <NavLink to="/" exact activeClassName="home_selected">
                    Start
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/news" exact activeClassName="news_selected">
                    News
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/contact"
                    exact
                    activeClassName="contact_selected"
                  >
                    Kontakt
                  </NavLink>
                </li>
              </ul>
            </nav>
          </header>
          <section>
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/news" component={News} />
              <Route path="/contact" component={Contact} />
              <Route component={ErrorPage} />
            </Switch>
          </section>
        </div>
      </Router>
    );
  }
}

export default App;
