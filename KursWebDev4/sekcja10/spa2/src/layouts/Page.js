import React from "react";
import { Route, Switch } from "react-router-dom";

import HomePage from "../pages/HomePage";
import ProductsPage from "../pages/ProductsPage";
import ContactsPage from "../pages/ContactsPage";
import AdminPage from "../pages/AdminPage";
import ErrorPage from "../pages/ErrorPage";

const Page = () => {
  return (
    <Switch>
      <Route path="/" exact component={HomePage} />
      <Route path="/products" component={ProductsPage} />
      <Route path="/contacts" component={ContactsPage} />
      <Route path="/admin" component={AdminPage} />
      <Route component={ErrorPage} />
    </Switch>
  );
};

export default Page;
