import React from "react";
import { Route, Switch } from "react-router-dom";

import header1 from "../images/header1.jpg";
import header2 from "../images/header2.jpg";
import header3 from "../images/header3.jpg";

import "../styles/Header.css";

const Header = () => {
  return (
    <Switch>
      <Route path="/" render={() => <img src={header1} alt="img1" />} />
      <Route path="/contacts" render={() => <img src={header1} alt="img1" />} />
      <Route path="/products" render={() => <img src={header2} alt="img2" />} />
      <Route path="/admin" render={() => <img src={header3} alt="img3" />} />
      <Route render={() => <img src={header1} alt="404" />} />
    </Switch>
  );
};

export default Header;
