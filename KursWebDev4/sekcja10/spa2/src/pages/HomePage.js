import React from "react";

import Article from "../components/Article";

const articles = [
  {
    id: 1,
    title: "tytul 1",
    author: "kowalski",
    text:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est consequuntur eos voluptas quos odio dolorum nostrum fuga quidem quae fugiat sunt a vitae hic, rem ex eius. Ad, illo dolorum."
  },
  {
    id: 2,
    title: "tytul 2",
    author: "kowalski",
    text:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est consequuntur eos voluptas quos odio dolorum nostrum fuga quidem quae fugiat sunt a vitae hic, rem ex eius. Ad, illo dolorum."
  },
  {
    id: 3,
    title: "tytul 3",
    author: "kowalski",
    text:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est consequuntur eos voluptas quos odio dolorum nostrum fuga quidem quae fugiat sunt a vitae hic, rem ex eius. Ad, illo dolorum."
  }
];

const HomePage = () => {
  const artList = articles.map(article => (
    <Article key={article.id} article={article} />
  ));
  return <div className="home">{artList}</div>;
};
export default HomePage;
