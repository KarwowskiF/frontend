import React from "react";

const Article = props => {
  const { text, author, title } = props.article;
  return (
    <article>
      <h3>{title}</h3>
      <span>{author}</span>
      <p>{text}</p>
    </article>
  );
};

export default Article;
