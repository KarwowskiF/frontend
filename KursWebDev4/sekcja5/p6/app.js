const Cash = props => {
  const value = ((props.cash / props.ratio) * props.price).toFixed(2);

  return (
    <div>
      {props.title} {props.cash <= 0 ? "" : value}
    </div>
  );
};

class ExchangeCounter extends React.Component {
  state = {
    amount: "",
    product: "gas"
  };

  static defaultProps = {
    currencies: [
      { id: 1, name: "dollar", ratio: 3.6, title: "Wartość w dolarach: " },
      { id: 2, name: "euro", ratio: 4.1, title: "Wartość w euro: " },
      { id: 3, name: "pound", ratio: 4.7, title: "Wartość w funtach: " },
      { id: 4, name: "zloty", ratio: 1, title: "Wartość w złotówkach: " }
    ],
    prices: {
      electricity: 0.51,
      gas: 4.76,
      oranges: 3.79
    }
  };

  handleChange = e => {
    this.setState({
      amount: e.target.value
    });
  };

  handleSelect = e => {
    this.setState({
      product: e.target.value,
      amount: ""
    });
  };

  insertSuffix(select) {
    if (select === "electricity") return <em> kWh</em>;
    if (select === "gas") return <em> l</em>;
    if (select === "oranges") return <em> Kg</em>;
    else return null;
  }
  selectPrice(select) {
    return this.props.prices[select];
  }

  render() {
    const calculators = this.props.currencies.map(currency => (
      <Cash
        key={currency.id}
        ratio={currency.ratio}
        title={currency.title}
        cash={this.state.amount}
        price={this.selectPrice(this.state.product)}
      />
    ));

    return (
      <div className="app">
        <label>
          Wybierz produkt:
          <select value={this.state.product} onChange={this.handleSelect}>
            <option value="electricity">Prąd</option>
            <option value="gas">Benzyna</option>
            <option value="oranges">Pomarańcze</option>
          </select>
        </label>
        <br />
        <br />
        <label>
          <input
            type="number"
            value={this.state.amount}
            onChange={this.handleChange}
          />
          {this.insertSuffix(this.state.product)}
        </label>
        {calculators}
      </div>
    );
  }
}

ReactDOM.render(<ExchangeCounter />, document.getElementById("root"));
