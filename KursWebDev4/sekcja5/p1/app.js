const ValidationMessage = props => {
  const { txt } = props;
  return <p>{txt}</p>;
};

const OrderForm = props => {
  const { submit, isConfirmed, change } = props;
  return (
    <form onSubmit={submit}>
      <input type="checkbox" id="age" checked={isConfirmed} onChange={change} />
      <label htmlFor="age">Mam conajmniej 16 lat</label>
      <br />
      <button>Kup bilet</button>
    </form>
  );
};

class TicketShop extends React.Component {
  state = {
    isConfirmed: false,
    isFormSubmitted: false
  };

  handleCheckboxChange = () => {
    this.setState({
      isConfirmed: !this.state.isConfirmed,
      isFormSubmitted: false
    });
  };

  handleFormSubmit = e => {
    e.preventDefault();
    if (!this.state.isFormSubmitted)
      this.setState({
        isFormSubmitted: !this.state.isFormSubmitted
      });
  };

  displayMessage = () => {
    if (this.state.isFormSubmitted)
      if (this.state.isConfirmed) return <ValidationMessage txt="jest ok" />;
      else return <ValidationMessage txt="nie jest ok!" />;
    else return null;
  };

  render() {
    const { isConfirmed } = this.state;

    return (
      <div>
        <h1>Kup bilet na horror roku!</h1>
        <OrderForm
          change={this.handleCheckboxChange}
          submit={this.handleFormSubmit}
          checked={isConfirmed}
        />
        {this.displayMessage()}
      </div>
    );
  }
}

ReactDOM.render(<TicketShop />, document.getElementById("root"));
