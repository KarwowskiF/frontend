import React, { Component } from "react";
import "./App.css";

class App extends Component {
  state = {
    userName: "",
    email: "",
    password: "",
    accept: false,

    message: "",

    errors: {
      userName: false,
      email: false,
      password: false,
      accept: false
    }
  };
  messages = {
    username_incorrect:
      " Nazwa musi być dłuższa niż 10 znaków i nie może zawierać spacji",
    email_incorrect: "Brak @ w emailu",
    password_incorrect: "Hasło musi mieć 8 znaków",
    accept_incorrect: "Nie potwierdzona zgoda"
  };
  handleChange = e => {
    const name = e.target.name;
    const type = e.target.type;

    if (type === "text" || type === "password" || type === "email") {
      const value = e.target.value;
      this.setState({
        [name]: value
      });
    } else if (type === "checkbox") {
      const checked = e.target.checked;
      this.setState({
        [name]: checked
      });
    }
  };
  handleSubmit = e => {
    e.preventDefault();
    const validation = this.formValidation();
    if (validation.correct) {
      this.setState({
        userName: "",
        email: "",
        password: "",
        accept: false,
        message: "Formularz został wysłany",

        errors: {
          userName: false,
          email: false,
          password: false,
          accept: false
        }
      });
    } else {
      this.setState({
        errors: {
          userName: !validation.username,
          email: !validation.email,
          password: !validation.password,
          accept: !validation.accept
        }
      });
    }
  };

  formValidation = () => {
    let username = false;
    let email = false;
    let password = false;
    let accept = false;
    let correct = false;

    if (
      this.state.userName.length > 10 &&
      this.state.userName.indexOf(" ") === -1
    )
      username = true;
    if (this.state.email.indexOf("@") !== -1) email = true;
    if (this.state.password.length === 8) password = true;
    if (this.state.accept) accept = true;

    if (username && password && email && accept) correct = true;
    return {
      correct,
      username,
      email,
      password,
      accept
    };
  };

  componentDidUpdate() {
    if (this.state.message !== "") {
      setTimeout(() => this.setState({ message: "" }), 3000);
    }
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit} noValidate>
          <label htmlFor="user">
            Twoje imię:
            <input
              type="text"
              id="user"
              name="userName"
              value={this.state.userName}
              onChange={this.handleChange}
            />
            {this.state.errors.userName && (
              <span>{this.messages.username_incorrect}</span>
            )}
          </label>
          <label htmlFor="email">
            Twój adres e-mail:
            <input
              type="email"
              id="email"
              name="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
            {this.state.errors.email && (
              <span>{this.messages.email_incorrect}</span>
            )}
          </label>
          <label htmlFor="password">
            Twoje hasło:
            <input
              type="password"
              id="password"
              name="password"
              value={this.state.password}
              onChange={this.handleChange}
            />
            {this.state.errors.password && (
              <span>{this.messages.password_incorrect}</span>
            )}
          </label>
          <label htmlFor="accept">
            <input
              type="checkbox"
              id="accept"
              name="accept"
              checked={this.state.accept}
              onChange={this.handleChange}
            />
            Akceptuje regulamin.
          </label>
          {this.state.errors.accept && (
            <span>{this.messages.accept_incorrect}</span>
          )}
          <button>Zapisz się</button>
        </form>

        {this.state.message && <h3>{this.state.message}</h3>}
      </div>
    );
  }
}

export default App;
