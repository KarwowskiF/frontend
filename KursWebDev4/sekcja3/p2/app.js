const Header = () => {
  return <h1>Komponent</h1>;
};

class Blog extends React.Component {
  render() {
    return (
      <section>
        <h2>komponent klasowy</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae quia
          omnis quas quidem amet itaque est maiores error excepturi sit. Qui,
          doloribus consequatur. Exercitationem quisquam voluptatibus aliquid!
          Quia, aut laborum?
        </p>
      </section>
    );
  }
}

const App = () => {
  return (
    <div>
      <Header />
      <Blog />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
