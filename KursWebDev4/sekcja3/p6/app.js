class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      msgIsActive: false
    };
    this.handleMsgBtn = this.handleMsgBtn.bind(this);
  }

  handleMsgBtn() {
    this.setState({
      msgIsActive: !this.state.msgIsActive
    });
  }

  render() {
    const text =
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum possimus numquam nam sed. Accusamus, hic? Rerum nisi quos aliquid? Ratione nobis inventore assumenda tempora beatae vel natus, error mollitia. Sapiente.";
    return (
      <div>
        <button onClick={this.handleMsgBtn}>
          {this.state.msgIsActive ? "Ukryj" : "Pokaz"}
        </button>
        {/* {this.state.msgIsActive ? <p>{text}</p> : null} */}
        {/* <p>{this.state.msgIsActive && text}</p> */}
        {this.state.msgIsActive && <p>{text}</p>}
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
