class App extends React.Component {
  state = {
    count: 0,
    result: this.props.result
  };

  handleMathClick = (type, number) => {
    if (type === "minus") {
      this.setState(prevState => ({
        count: prevState.count + 1,
        result: prevState.result - number
      }));
    } else if (type === "plus") {
      this.setState(prevState => ({
        count: prevState.count + 1,
        result: prevState.result + number
      }));
    } else if (type === "reset") {
      this.setState({
        count: 0,
        result: this.props.result
      });
    }
  };

  render() {
    return (
      <div>
        <MathButton
          name="-1"
          number="1"
          type="minus"
          click={this.handleMathClick}
        />
        <MathButton name="Reset" type="reset" click={this.handleMathClick} />
        <MathButton
          name="1"
          number="1"
          type="plus"
          click={this.handleMathClick}
        />
        <ResultPanel count={this.state.count} result={this.state.result} />
      </div>
    );
  }
}

const MathButton = props => {
  const number = parseInt(props.number);
  return (
    <button onClick={() => props.click(props.type, number)}>
      {props.name}
    </button>
  );
};

const ResultPanel = props => {
  return (
    <div>
      <h1>Liczba klikniec: {props.count}</h1>
      <h1>wynik: {props.result}</h1>
    </div>
  );
};

const startValue = 0;
ReactDOM.render(<App result={startValue} />, document.getElementById("root"));
