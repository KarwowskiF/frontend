class ShoppingList extends React.Component {
  state = {
    items1: "ogorki",
    items2: "sok",
    items3: "chleb"
  };
  render() {
    return (
      <div>
        <h1>lista zakupów</h1>
        <ul>
          <ItemList name={this.state.items1} />
          <ItemList name={this.state.items2} />
          <ItemList name={this.state.items3} />
        </ul>
      </div>
    );
  }
}

// const ItemList = props => {
//   return <li>{props.name}</li>;
// };
class ItemList extends React.Component {
  render() {
    return <li>{this.props.name}</li>;
  }
}

ReactDOM.render(<ShoppingList />, document.getElementById("root"));
