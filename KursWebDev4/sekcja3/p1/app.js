const header = <h1 className="title">Witaj na stronie!</h1>;

const classBig = "big";
const main = (
  <div>
    <h1 className="red">Pierwszy artykuł</h1>
    <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat laborum
      eum maxime provident esse ea soluta expedita fuga magni recusandae, est
      quas consectetur beatae numquam ratione fugit accusantium similique quis.
    </p>
  </div>
);

const footer = (
  <footer>
    <p className={classBig}>stopka</p>
  </footer>
);

const app = [header, main, footer];

ReactDOM.render(app, document.getElementById("root"));
