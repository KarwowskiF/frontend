class App extends React.Component {
  state = {
    value: ""
  };

  handleInputChange = e => {
    console.log(e.target.value);
    this.setState({ value: e.target.value });
  };

  handleClick = () => {
    this.setState({
      value: ""
    });
  };

  render() {
    return (
      <div>
        <input
          value={this.state.value}
          placeholder="wpisz..."
          onChange={this.handleInputChange}
          type="text"
        />
        <button onClick={this.handleClick}>Reset</button>
        <h1>{this.state.value.toUpperCase()}</h1>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
